#javascript_compile_game
/*
名称：javascript制作的编译器

作者：浙江省新昌县城西小学 唐明

联系QQ:147885198

功能：对输入的代码按照自定义的一种语法进行语法分析并检测语法错误，根据代码生成虚拟计算机代码，最后模拟执行这个计算机代码。

特色：结合游戏功能，用代码指挥屏幕上的小车完成任务。

更新时间：

2015-4-28：

	1、语法检查，生成虚拟代码，执行代码

	2、俯视地图的绘制，游戏完成与否的判定。

*/

//关键词列表

var keyword=['if','else','repeat'];

var sysfunc=['go','toleft','toright','jump','read','write','check','remove','color'];